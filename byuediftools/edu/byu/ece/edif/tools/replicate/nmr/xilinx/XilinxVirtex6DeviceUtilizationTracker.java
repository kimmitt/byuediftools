package edu.byu.ece.edif.tools.replicate.nmr.xilinx;

import edu.byu.ece.edif.arch.xilinx.parts.XilinxPartLookup;
import edu.byu.ece.edif.core.EdifCell;
import edu.byu.ece.edif.tools.replicate.nmr.OverutilizationEstimatedStopException;
import edu.byu.ece.edif.tools.replicate.nmr.OverutilizationHardStopException;

public class XilinxVirtex6DeviceUtilizationTracker extends XilinxDeviceUtilizationTracker {

	public XilinxVirtex6DeviceUtilizationTracker(EdifCell cell, String part)
		throws OverutilizationEstimatedStopException, OverutilizationHardStopException, IllegalArgumentException {
			this(cell, part, DEFAULT_MERGE_FACTOR, DEFAULT_OPTIMIZATION_FACTOR, DEFAULT_DESIRED_UTILIZATION_FACTOR);
	}

	public XilinxVirtex6DeviceUtilizationTracker(EdifCell cell, String part, double mergeFactor,
			double optimizationFactor, double desiredUtilizationFactor) throws OverutilizationEstimatedStopException,
			OverutilizationHardStopException, IllegalArgumentException {

		super(mergeFactor, optimizationFactor, desiredUtilizationFactor);
		part = XilinxPartLookup.getPartFromPartName(part).getPartNameNoSpeedGrade();

		if (part.compareToIgnoreCase("XC6VLX240TFF1156") == 0) {
			_init(cell, 150720, 75360, 832, 32, 0, 32, 720, 768, 0, 4, 12, 2, 1, 1, 8);
		}
		else {
			throw new IllegalArgumentException("Part name " + part
				+ " unimplemented Xilinx Virtex6 technology group.");
		}
	}

	protected void _init(EdifCell cell, int maxLUTs, int maxFFs, int maxBlockRAMs, int maxBUFG, int maxMult,
			int maxDCM, int maxIO, int maxDSPs, int maxPPC, int maxEthernet, int maxMGT, int maxPCIe, int maxICAP,
			int maxFrameECC, int maxPLL) throws OverutilizationEstimatedStopException,
			OverutilizationHardStopException, IllegalArgumentException {
		addResourceForTracking(XilinxResourceMapper.LUT, 0.0, maxLUTs);
		addResourceForTracking(XilinxResourceMapper.FF, 0.0, maxFFs);
		addResourceForTracking(XilinxResourceMapper.BRAM, 0.0, maxBlockRAMs);
		addResourceForTracking(XilinxResourceMapper.MULT, 0.0, maxMult);
		addResourceForTracking(XilinxResourceMapper.DCM, 0.0, maxDCM);
		addResourceForTracking(XilinxResourceMapper.IO, 0.0, maxIO);
		addResourceForTracking(XilinxResourceMapper.RES, 0.0, maxIO); // One per IOB
		addResourceForTracking(XilinxResourceMapper.BUFG, 0.0, maxBUFG);
		addResourceForTracking(XilinxResourceMapper.IBUFG, 0.0, maxBUFG);
		addResourceForTracking(XilinxResourceMapper.DSP, 0.0, maxDSPs);
		addResourceForTracking(XilinxResourceMapper.ICAP, 0.0, maxICAP);
		addResourceForTracking(XilinxResourceMapper.FRAME_ECC, 0.0, maxFrameECC);
		addResourceForTracking(XilinxResourceMapper.PPC, 0.0, maxPPC);
		addResourceForTracking(XilinxResourceMapper.ETHERNET, 0.0, maxEthernet);
		addResourceForTracking(XilinxResourceMapper.TRANSCEIVER, 0.0, maxMGT);
		addResourceForTracking(XilinxResourceMapper.PCIE, 0.0, maxPCIe);
		addResourceForTracking(XilinxResourceMapper.PLL, 0.0, maxPLL);
		super._init(cell);
	}
}
